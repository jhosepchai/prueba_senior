

console.log(gradingStudents([60,67,38,33,105, 100, 0, -1, 20, 15]));


function gradingStudents(grades = new Array<Int32Array> []) {
    const nextPrime = (number) =>{
        let flag = true;
        while (flag) {
            number++;
            if (number % 5 == 0) {
                return number
            }
        }
    }
    let new_grades = [];
    let next_prime = [];

    if (grades[0] >= 1 && grades[0] <= 60) {
        grades.shift();//elimina el primer elemento ya que es el numero de estudiantes
        grades.forEach(grade => {
            if (grade >= 38) {
                if (grade > 100) {
                    new_grades.push(100)
                } else {
                    next_prime = nextPrime(grade)
                    if (next_prime - grade   < 3) {
                        new_grades.push(next_prime)
                    } else {
                        new_grades.push(grade)
                    }
                }
            } else {
                new_grades.push(grade < 0 ?  0 : grade)
            }
        });
        return new_grades;        
    }
    return [];
}